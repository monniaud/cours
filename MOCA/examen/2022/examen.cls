% -*- coding: utf-8; -*-
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{examen}
\LoadClass[a4paper,11pt]{article}

\RequirePackage{ifthen}
\RequirePackage{comment}
\RequirePackage{amsthm}

\newboolean{corrige}
\setboolean{corrige}{false}
\newboolean{utf8}
\setboolean{utf8}{false}

\DeclareOption{corrige}{\setboolean{corrige}{true}}
\DeclareOption{utf8}{\setboolean{utf8}{true}}
\ProcessOptions

\ifthenelse{\boolean{utf8}}{\ifxetex\relax\else\RequirePackage[utf8]{inputenc}\fi}{}

\ifthenelse{\boolean{corrige}}{
\newenvironment{solution}{\noindent\emph{Solution~:} }{\qed\bigskip\par}}{
\excludecomment{solution}}

\newtheorem{question}{Question}[section]
