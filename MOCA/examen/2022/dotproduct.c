#include <stdio.h>
#include <stdlib.h>

/* Calcul du produit scalaire de deux vecteurs */
double dotproduct(int n, double *a, double *b) {
  double s = 0.0;
  int i=0;
  while (i < n) {
    s += a[i] * b[i];
    i++;
  }
  return s;
}

/* Calcul du produit scalaire de deux vecteurs,
   deux produits-additions par iteration de boucle */
double dotproduct2(int n, double *a, double *b) {
  double s = 0.0;
  int i=0;
  while (i < n) {
    s += a[i] * b[i];
    i++;
    s += a[i] * b[i];
    i++;
  }
  return s;
}

#define N 15

int main() {
  double a[N], b[N];
  srand48(0xBEEF);
  for(int i=0; i<N; i++) {
    a[i] = drand48();
    b[i] = drand48();
  }
  double r1 = dotproduct(N, a, b);
  double r2 = dotproduct2(N, a, b);
  printf("%f %f %d\n", r1, r2, r1==r2);
}
