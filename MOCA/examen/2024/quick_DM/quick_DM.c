#include <stdio.h>

int calcul(int *t) {
  int u[10], v[10], r = 0;
  for(int i=0; i<=10; i++) {
    u[i] = t[i] * 2;
  }
  for(int i=0; i<10; i++) {
    v[i] = u[i] + 1;
  }
  for(int i=0; i<10; i++) {
    r = r + v[i];
  }
  return r;
}

static int table[11] = {1,2,3,4,5,6,7,8,9,10};

int main() {
  printf("%d\n", calcul(table));
  return 0;
}
