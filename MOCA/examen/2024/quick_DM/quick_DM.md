Considérons le programme suivant.

```
#include <stdio.h>

int calcul(int *t) {
  int u[10], v[10], r = 0;
  for(int i=0; i<=10; i++) {
    u[i] = t[i] * 2;
  }
  for(int i=0; i<10; i++) {
    v[i] = u[i] + 1;
  }
  for(int i=0; i<10; i++) {
    r = r + v[i];
  }
  return r;
}

static int table[11] = {1,2,3,4,5,6,7,8,9,10};

int main() {
  printf("%d\n", calcul(table));
  return 0;
}
```

Ce programme compile avec `gcc -Wall` ou `clang -Wall` sans aucun message d'avertissement. Il s'exécute alors sans provoquer d'erreur et affiche `120`.

On exécute alors ce programme avec Valgrind, toujours pas d'erreur affichée, toujours `120` imprimé.

On recompile ce programme avec `-fsanitize=address` et on obtient grosso modo ceci :
```
==102084==ERROR: AddressSanitizer: stack-buffer-overflow on address 0x7ffc626a5c98 at pc 0x561c628ee3be bp 0x7ffc626a5c20 sp 0x7ffc626a5c10
WRITE of size 4 at 0x7ffc626a5c98 thread T0
    #0 0x561c628ee3bd in calcul (…)
    #1 0x561c628ee5e5 in main (…)
    #2 0x7f93303b5082 in __libc_start_main ../csu/libc-start.c:308
    #3 0x561c628ee18d in _start (/home/monniaux/work/cours/MOCA/examen/2024/quick_DM+0x118d)

Address 0x7ffc626a5c98 is located in stack of thread T0 at offset 72 in frame
    #0 0x561c628ee258 in calcul (/home/monniaux/work/cours/MOCA/examen/2024/quick_DM+0x1258)

  This frame has 2 object(s):
    [32, 72) 'u' (line 4) <== Memory access at offset 72 overflows this variable
    [112, 152) 'v' (line 4)
```

==Question 1==
Expliquez cette erreur : pourquoi _address sanitizer_ proteste-t-il ?

==Question 2==
Pourquoi Valgrind n'a-t-il pas protesté ?

Considérons la fonction suivante :
```
int addition(int x, int y) {
  return x + y;
}
```

==Question 3==
Cette fonction peut-elle réaliser un comportement indéfini ? Si oui, lequel ?

Considérons la fonction suivante :
```
int ajout(int x) {
  if (x >= 1000) return 1000;
  return x + 1;
}
```

==Question 3==
Cette fonction peut-elle réaliser un comportement indéfini ? Si oui, lequel ?
