#include <stdlib.h>
#include <stdio.h>

int recherche(int x, int *t, int n) {
  int i=0;
  while (t[i] != x && i < n) i++;
  if (i >= n) return -1;
  return i;
}

#define N 40

int main() {
#ifdef MALLOC
  int *buf = malloc(N*sizeof(int));
#else
  int buf[N];
#endif
  for(int i=0; i<N; i++) buf[i]=i+1;
  printf("rech   4 = %d\n", recherche(4, buf, N));
  printf("rech -10 = %d\n", recherche(-10, buf, N));
#ifdef MALLOC
  free(buf);
#endif
}
