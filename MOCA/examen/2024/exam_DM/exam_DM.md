La fonction suivante est censée rechercher la première occurence de `x` dans le tableau `t` de taille `n`.

```
int recherche(int x, int *t, int n) {
  int i=0;
  while (t[i] != x && i < n) i++;
  if (i >= n) return -1;
  return i;
}
```

# Question 1
Cette procédure est-elle correcte ? Justifiez. Si vous pensez qu'elle ne l'est pas, proposez un correctif.

On teste cette procédure à l'aide de :
```

#define N 40

int main() {
#ifdef MALLOC
  int *buf = malloc(N*sizeof(int));
#else
  int buf[N];
#endif
  for(int i=0; i<N; i++) buf[i]=i+1;
  printf("rech   4 = %d\n", recherche(4, buf, N));
  printf("rech -10 = %d\n", recherche(-10, buf, N));
#ifdef MALLOC
  free(buf);
#endif
}
```

On rappelle que `#ifdef MALLOC` / `#else` / `#endif` permet de compiler ou non les lignes de code suivant qu'on ait ou non passé `-DMALLOC` en ligne de commande du compilateur.

# Question 2
Avec `-DMALLOC`, ce programme produit l'erreur suivante dans Valgrind :
```
==7821== Invalid read of size 4
==7821==    at 0x40116D: recherche (in /home/monniaux/work/cours/MOCA/examen/2024/exam_DM/exam_DM)
==7821==    by 0x401255: main (in /home/monniaux/work/cours/MOCA/examen/2024/exam_DM/exam_DM)
==7821==  Address 0x4a7b0e0 is 0 bytes after a block of size 160 alloc'd
==7821==    at 0x483B7F3: malloc (in /usr/lib/x86_64-linux-gnu/valgrind/vgpreload_memcheck-amd64-linux.so)
==7821==    by 0x4011E8: main (in /home/monniaux/work/cours/MOCA/examen/2024/exam_DM/exam_DM)
```

Expliquez cette erreur (pourquoi un `block of size 160` etc.).

# Question 3
Le même programme, exécuté hors de Valgrind, ne produit pourtant aucune erreur. Expliquez ce mystère.


# Question 4
Sans `-DMALLOC`, ce programme produit l'erreur suivante dans Valgrind :
```
==7914== Conditional jump or move depends on uninitialised value(s)
==7914==    at 0x401158: recherche (in /home/monniaux/work/cours/MOCA/examen/2024/exam_DM/exam_DM)
==7914==    by 0x401242: main (in /home/monniaux/work/cours/MOCA/examen/2024/exam_DM/exam_DM)
```

Expliquez cette erreur.

# Question 5
Le même programme, exécuté hors de Valgrind, ne produit pourtant aucune erreur. Expliquez ce mystère.

# Question 6
Nous recompilons le programme avec la librairie `duma`, qui propose une autre implémentation correcte de `malloc()` / `free()`.

Cette fois-ci, l'exécution du programme (hors de Valgrind) produit une erreur de segmentation à la ligne
```
while (t[i] != x && i < n) i++;
```

Expliquez ce mystère.
