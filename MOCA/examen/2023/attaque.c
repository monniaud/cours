#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

void miaou(void) {
  puts("COINCOIN");
  exit(1);
}

void toto(uint64_t *t) {
  t[-1] = (uint64_t) &miaou;
}

int main() {
  uint64_t t[10];
  toto(t);
  puts("OUAF");
  return 0;
}
