#include <klee/klee.h>

int toto(int n, int k) {
  if (n <= 0) return -1;
  if (k < 0 || k > n) return -2;
  int t[n];
  t[0] = 1;
  for(int i=1; i<n; i++) {
    t[i] = t[i-1] + i;
  }
  return t[k];
}

int main(int argc, char **argv) {
  int n=1000, k;
  klee_make_symbolic(&k, sizeof(k), "toto");
  return toto(n, k);
}
